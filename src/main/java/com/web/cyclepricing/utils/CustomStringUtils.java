package com.web.cyclepricing.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomStringUtils {

    public static final String dateFormat = "dd-MM-yyyy";
    public static Long getEpochTime(String dateInString) {

        Long epochTime = Long.valueOf(0);
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try {
            Date inputDate = formatter.parse(dateInString);
            epochTime = inputDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return epochTime;
    }

    public static Date stringToDate(String dateInString) {
        Date inputDate = null;
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        try {
            inputDate = formatter.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return inputDate;
    }

    public static boolean compareDates(String date) {
        Date minDate = stringToDate("01-01-2016");
        Date maxDate = stringToDate("30-11-2016");
        Date d = stringToDate(date);
        return d.compareTo(minDate) >= 0 && d.compareTo(maxDate) <= 0;
    }

    public static String genKey(Long date) {
        Date minDate = stringToDate("01-01-2016");
        Date maxDate = stringToDate("30-11-2016");
        Date d = new Date(date);
        String key = "dec-2016";
        if (d.compareTo(minDate) >= 0 && d.compareTo(maxDate) <= 0)
            key = "jan-2016";
        return key;
    }

    public static void main(String[] args) {
        String date = "17-05-2016";
        String date1 = "17-12-2016";
        System.out.println("date == "+getEpochTime(date));
        System.out.println("date1 == "+getEpochTime(date1));
        Date realDate = new Date(getEpochTime(date1));
        System.out.println("real date1 == "+realDate);
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        String strDate = formatter.format(realDate);
        System.out.println("strDate = "+strDate);
        System.out.println("compareDates = "+compareDates("17-12-2016"));

    }
}
