package com.web.cyclepricing.services;

import com.web.cyclepricing.dao.ChainAssemblyDAO;
import com.web.cyclepricing.model.ChainAssembly;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChainAssemblyService {

	private ChainAssemblyDAO chainAssemblyDAO;

	@Autowired
	public ChainAssemblyService(ChainAssemblyDAO chainAssemblyDAO) {
		this.chainAssemblyDAO = chainAssemblyDAO;
	}

	public double getPrice(ChainAssembly chainAssembly) {
		return chainAssembly.getPrice();
	}

	public void calculatePrice(Long date, ChainAssembly chainAssembly) {
		double sizePrice = chainAssemblyDAO.getChainSizePrice(chainAssembly.getChainSize(), date);

		double speedPrice = chainAssemblyDAO.getChainSpeedPrice(chainAssembly.getSpeed(), date);

		double totalPrice = sizePrice + speedPrice;
		chainAssembly.setPrice(totalPrice);
	}

}
