package com.web.cyclepricing.services;

import com.web.cyclepricing.dao.HandleDAO;
import com.web.cyclepricing.model.Handle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HandleService {

	private HandleDAO handleDAO;

	@Autowired
	public HandleService(HandleDAO handleDAO) {
		this.handleDAO = handleDAO;
	}

	public double getPrice(Handle handle) {
		return handle.getPrice();
	}

	public void calculatePrice(Long date, Handle handle) {
		double spokePrice = handleDAO.getHandlePrice(handle.getType(), date);

		double shockLockPrice = handleDAO.getShocklockPrice(handle.isShockLock(), date);
		double totalPrice = spokePrice + shockLockPrice;
		handle.setPrice(totalPrice);
	}

}
