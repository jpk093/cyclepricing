package com.web.cyclepricing.services;

import com.web.cyclepricing.dao.SeatDAO;
import com.web.cyclepricing.model.Seat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SeatService {

	private SeatDAO seatDAO;

	@Autowired
	public SeatService(SeatDAO seatDAO) {
		this.seatDAO = seatDAO;
	}

	public double getPrice(Seat seat) {
		return seat.getPrice();
	}

	public void calculatePrice(Long date, Seat seat) {
		double price = seatDAO.getSeatPrice(seat.getType(), date);
		double totalPrice = price;
		seat.setPrice(totalPrice);
	}

}
