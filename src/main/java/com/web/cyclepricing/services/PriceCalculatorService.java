package com.web.cyclepricing.services;

import com.web.cyclepricing.model.Bicycle;
import com.web.cyclepricing.utils.CustomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class PriceCalculatorService {

    private static final Logger log = LoggerFactory.getLogger(PriceCalculatorService.class);

    private WheelService wheelService;
    private HandleService handleService;
    private FrameService frameService;
    private ChainAssemblyService chainAssemblyService;
    private SeatService seatService;

    @Autowired
    public PriceCalculatorService(WheelService wheelService, HandleService handleService,
                                     FrameService frameService, ChainAssemblyService chainAssemblyService, SeatService seatService) {
        this.wheelService = wheelService ;
        this.handleService = handleService;
        this.frameService = frameService;
        this.chainAssemblyService = chainAssemblyService;
        this.seatService = seatService;
    }

    @Async
    public CompletableFuture<List<Bicycle>> getAllBicycle(List<Bicycle> bicycles){
        log.info("Inside getAllBicycle()");
        final long start = System.currentTimeMillis();

        List<Bicycle> bicycleList = this.getAllPriceCalculator(bicycles);

        log.info("getting a list of bicycles of size {} records", bicycleList.size());

        log.info("Elapsed time: {}", (System.currentTimeMillis() - start));
        return CompletableFuture.completedFuture(bicycleList);
    }


    public List<Bicycle> getAllPriceCalculator(List<Bicycle> bicycles) {
        log.info("getAllPriceCalculator() bicycles size ["+bicycles.size()+"]");

        List<Bicycle> bicycleList = new ArrayList<Bicycle>();

        bicycles.forEach( bicycle -> {
            String date = bicycle.getDate();
            Long epochDate = CustomStringUtils.getEpochTime(date);

            wheelService.calculatePrice(epochDate, bicycle.getWheel());

            frameService.calculatePrice(epochDate, bicycle.getFrame());

            handleService.calculatePrice(epochDate, bicycle.getHandle());

            chainAssemblyService.calculatePrice(epochDate, bicycle.getChainAssembly());
            seatService.calculatePrice(epochDate, bicycle.getSeat());

            bicycle.calculatePrice();
            bicycleList.add(bicycle);
        });

        return bicycleList;
    }


    public Bicycle getPriceCalculator(Bicycle bicycle) {
        log.info("getPriceCalculator()");

        String date = bicycle.getDate();
        Long epochDate = CustomStringUtils.getEpochTime(date);

        wheelService.calculatePrice(epochDate, bicycle.getWheel());

        frameService.calculatePrice(epochDate, bicycle.getFrame());

        handleService.calculatePrice(epochDate, bicycle.getHandle());

        chainAssemblyService.calculatePrice(epochDate, bicycle.getChainAssembly());
        seatService.calculatePrice(epochDate, bicycle.getSeat());

        bicycle.calculatePrice();

        return bicycle;
    }
}
