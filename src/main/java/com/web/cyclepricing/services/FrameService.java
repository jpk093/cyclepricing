package com.web.cyclepricing.services;

import com.web.cyclepricing.dao.FrameDAO;
import com.web.cyclepricing.model.Frame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FrameService {

	private FrameDAO frameDAO;

	@Autowired
	public FrameService(FrameDAO frameDAO) {
		this.frameDAO = frameDAO;
	}

	public double getPrice(Frame frame) {
		return frame.getPrice();
	}

	public void calculatePrice(Long date, Frame frame) {
		double spokePrice = frameDAO.getFramePrice(frame.getType(), date);
		double totalPrice = spokePrice;
		frame.setPrice(totalPrice);
	}

}
