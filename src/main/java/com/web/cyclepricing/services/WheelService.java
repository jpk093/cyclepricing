package com.web.cyclepricing.services;

import com.web.cyclepricing.dao.WheelDAO;
import com.web.cyclepricing.model.Wheel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WheelService {

	private WheelDAO wheelDAO;

	@Autowired
	public WheelService(WheelDAO wheelDAO) {
		this.wheelDAO = wheelDAO;
	}

	public double getPrice(Wheel wheel) {
		return wheel.getPrice(); 
	}

	public void calculatePrice(Long date, Wheel wheel) {

		double spokePrice = wheelDAO.getSpokePrice(wheel.getSpoke(), date);

		double rimPrice = wheelDAO.getRimPrice(wheel.getRim(), date);

		double tubePrice = wheelDAO.getTubePrice(wheel.getTube(), date);

		double tyrePrice = wheelDAO.getTyrePrice(wheel.getTyre(), date);

		double totalPrice = spokePrice + rimPrice + tubePrice + tyrePrice;

		wheel.setPrice(totalPrice);

	}

}
