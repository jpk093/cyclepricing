package com.web.cyclepricing.api;

import com.web.cyclepricing.model.Bicycle;
import com.web.cyclepricing.services.*;
import com.web.cyclepricing.utils.CustomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("api")
public class PriceCalculatorController {

    private static final Logger log = LoggerFactory.getLogger(PriceCalculatorController.class);

    private PriceCalculatorService priceCalculatorService;

    @Autowired
    public PriceCalculatorController(PriceCalculatorService priceCalculatorService) {
        this.priceCalculatorService = priceCalculatorService ;
    }


    @PostMapping("priceCalculator")
    ResponseEntity<Bicycle> getPriceCalculator(@RequestBody Bicycle bicycle) {
        log.info("getPriceCalculator()");
        return new ResponseEntity<Bicycle>(priceCalculatorService.getPriceCalculator(bicycle), HttpStatus.OK);
    }

    @PostMapping("allPriceCalculator")
    CompletableFuture<ResponseEntity> getAllPriceCalculator(@RequestBody List<Bicycle> bicycles) {
        log.info("getAllPriceCalculator()");
        return priceCalculatorService.getAllBicycle(bicycles).<ResponseEntity>thenApply(ResponseEntity::ok);
    }
}
