package com.web.cyclepricing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CyclepricingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CyclepricingApplication.class, args);
    }

}
