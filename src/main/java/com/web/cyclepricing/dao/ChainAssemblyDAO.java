package com.web.cyclepricing.dao;

import com.web.cyclepricing.utils.CustomStringUtils;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class ChainAssemblyDAO {

	private Map<String, Double> chain_size_ten_price;
	private Map<String, Double> chain_size_eight_price;
	private Map<String, Double> chain_size_six_price;

	private Map<String, Double> chain_speed_ten_price;
	private Map<String, Double> chain_speed_eight_price;
	private Map<String, Double> chain_speed_six_price;

	public ChainAssemblyDAO() {
		setChainPrice();
	}

	private void setChainPrice() {
		chain_size_ten_price = new HashMap<>();
		chain_size_ten_price.put("dec-2016", 2222.00);
		chain_size_ten_price.put("jan-2016", 1111.00);

		chain_size_eight_price = new HashMap<>();
		chain_size_eight_price.put("dec-2016", 1000.00);
		chain_size_eight_price.put("jan-2016", 2000.00);

		chain_size_six_price = new HashMap<>();
		chain_size_six_price.put("dec-2016", 1000.00);
		chain_size_six_price.put("jan-2016", 2000.00);

		chain_speed_ten_price = new HashMap<>();
		chain_speed_ten_price.put("dec-2016", 1000.00);
		chain_speed_ten_price.put("jan-2016", 2000.00);

		chain_speed_eight_price = new HashMap<>();
		chain_speed_eight_price.put("dec-2016", 1000.00);
		chain_speed_eight_price.put("jan-2016", 2000.00);

		chain_speed_six_price = new HashMap<>();
		chain_speed_six_price.put("dec-2016", 100.00);
		chain_speed_six_price.put("jan-2016", 200.00);

	}

	public Double getChainSizePrice(int part, Long date) {
		Double price = Double.valueOf(0);

		if (part == 10) {

			for (Map.Entry<String, Double> entry : chain_size_ten_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part == 8) {

			for (Map.Entry<String, Double> entry : chain_size_eight_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (6 == part) {

			for (Map.Entry<String, Double> entry : chain_size_six_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		}
		return price;
	}

	public Double getChainSpeedPrice(int part, Long date) {
		Double price = Double.valueOf(0);

		if (part == 10) {

			for (Map.Entry<String, Double> entry : chain_speed_ten_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part == 8) {

			for (Map.Entry<String, Double> entry : chain_speed_eight_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (6 == part) {

			for (Map.Entry<String, Double> entry : chain_speed_six_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		}
		return price;
	}

}
