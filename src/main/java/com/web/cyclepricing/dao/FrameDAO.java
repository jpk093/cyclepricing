package com.web.cyclepricing.dao;

import com.web.cyclepricing.enums.FrameType;
import com.web.cyclepricing.utils.CustomStringUtils;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.HashMap;

@Repository
public class FrameDAO {

	private Map<String, Double> frame_ordinary_price;
	private Map<String, Double> frame_cantilever_price;
	private Map<String, Double> frame_diamond_price;

	private Map<String, Double> frame_recumbent_price;
	private Map<String, Double> frame_prone_price;

	private Map<String, Double> frame_stepthrough_price;

	public FrameDAO() {
		setFramePrice();
	}

	private void setFramePrice() {
		frame_ordinary_price = new HashMap<>();
		frame_ordinary_price.put("dec-2016", 2000.00);
		frame_ordinary_price.put("jan-2016", 1500.00);

		frame_cantilever_price = new HashMap<>();
		frame_cantilever_price.put("dec-2016", 1000.00);
		frame_cantilever_price.put("jan-2016", 2000.00);

		frame_diamond_price = new HashMap<>();
		frame_diamond_price.put("dec-2016", 1000.00);
		frame_diamond_price.put("jan-2016", 2000.00);

		frame_recumbent_price = new HashMap<>();
		frame_recumbent_price.put("dec-2016", 1000.00);
		frame_recumbent_price.put("jan-2016", 2000.00);

		frame_prone_price = new HashMap<>();
		frame_prone_price.put("dec-2016", 1000.00);
		frame_prone_price.put("jan-2016", 2000.00);

		frame_stepthrough_price = new HashMap<>();
		frame_stepthrough_price.put("dec-2016", 1000.00);
		frame_stepthrough_price.put("jan-2016", 2000.00);

	}

	public Double getFramePrice(String part, Long date) {
		Double price = Double.valueOf(0);

		if (part.equalsIgnoreCase(FrameType.ORDINARY.toString())) {

			for (Map.Entry<String, Double> entry : frame_ordinary_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(FrameType.CANTILEVER.toString())) {

			for (Map.Entry<String, Double> entry : frame_cantilever_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(FrameType.DIAMOND.toString())) {

			for (Map.Entry<String, Double> entry : frame_diamond_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(FrameType.RECUMBENT.toString())) {

			for (Map.Entry<String, Double> entry : frame_recumbent_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(FrameType.PRONE.toString())) {

			for (Map.Entry<String, Double> entry : frame_prone_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(FrameType.STEPTHROUGH.toString())) {

			for (Map.Entry<String, Double> entry : frame_stepthrough_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		}
		return price;
	}

}
