package com.web.cyclepricing.dao;

import com.web.cyclepricing.enums.Rim;
import com.web.cyclepricing.enums.Spoke;
import com.web.cyclepricing.enums.Tube;
import com.web.cyclepricing.enums.Tyre;
import com.web.cyclepricing.utils.CustomStringUtils;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.HashMap;

@Repository
public class WheelDAO {

	private Map<String, Double> rim_steel_price;
	private Map<String, Double> rim_alloy_price;
	private Map<String, Double> rim_chrome_price;

	private Map<String, Double> tangentlacing_spoke_price;
	private Map<String, Double> wheelbuilding_spoke_price;

	private Map<String, Double> tube_tube_price;
	private Map<String, Double> tubeless_tube_price;

	private Map<String, Double> clincher_tyre_price;
	private Map<String, Double> tubular_tyre_price;

	public WheelDAO() {
		setRimPrice();
		setSpokePrice();
		setTubePrice();
		setTyrePrice();
	}

	private void setTyrePrice() {
		clincher_tyre_price = new HashMap<>();
		clincher_tyre_price.put("dec-2016", 230.00);
		clincher_tyre_price.put("jan-2016", 200.00);

		tubular_tyre_price = new HashMap<>();
		tubular_tyre_price.put("dec-2016", 230.00);
		tubular_tyre_price.put("jan-2016", 200.00);

	}

	private void setTubePrice() {

		tube_tube_price = new HashMap<>();
		tube_tube_price.put("dec-2016", 2000.00);
		tube_tube_price.put("jan-2016", 1500.00);

		tubeless_tube_price = new HashMap<>();
		tubeless_tube_price.put("dec-2016", 1000.00);
		tubeless_tube_price.put("jan-2016", 2000.00);

	}

	private void setSpokePrice() {
		tangentlacing_spoke_price = new HashMap<>();
		tangentlacing_spoke_price.put("dec-2016", 2000.00);
		tangentlacing_spoke_price.put("jan-2016", 1500.00);

		wheelbuilding_spoke_price = new HashMap<>();
		wheelbuilding_spoke_price.put("dec-2016", 1000.00);
		wheelbuilding_spoke_price.put("jan-2016", 2000.00);

	}

	private void setRimPrice() {

		rim_steel_price = new HashMap<>();
		rim_steel_price.put("dec-2016", 2000.00);
		rim_steel_price.put("jan-2016", 1500.00);

		rim_alloy_price = new HashMap<>();
		rim_alloy_price.put("dec-2016", 1000.00);
		rim_alloy_price.put("jan-2016", 2000.00);

		rim_chrome_price = new HashMap<>();
		rim_chrome_price.put("dec-2016", 3000.00);
		rim_chrome_price.put("jan-2016", 4000.00);

	}

	public Double getRimPrice(String part, Long date) {
		Double price = Double.valueOf(0);

		if (part.equalsIgnoreCase(Rim.ALLOY.toString())) {

			for (Map.Entry<String, Double> entry : rim_alloy_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(Rim.CHROME.toString())) {

			for (Map.Entry<String, Double> entry : rim_chrome_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(Rim.STEEL.toString())) {

			for (Map.Entry<String, Double> entry : rim_steel_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		}
		return price;
	}

	public Double getSpokePrice(String spoke, Long date) {
		Double price = Double.valueOf(0);
		if (spoke.equalsIgnoreCase(Spoke.TANGENTIALLACING.toString())) {
			for (Map.Entry<String, Double> entry : tangentlacing_spoke_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (spoke.equalsIgnoreCase(Spoke.WHEELBUILDING.toString())) {
			for (Map.Entry<String, Double> entry : wheelbuilding_spoke_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}

		}
		return price;
	}

	public Double getTubePrice(String parts, Long date) {
		Double price = Double.valueOf(0);
		if (parts.equalsIgnoreCase(Tube.TUBE.toString())) {
			for (Map.Entry<String, Double> entry : tube_tube_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (parts.equalsIgnoreCase(Tube.TUBELESS.toString())) {
			for (Map.Entry<String, Double> entry : tubeless_tube_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}

		}
		return price;
	}

	public Double getTyrePrice(String parts, Long date) {
		Double price = Double.valueOf(0);
		if (parts.equalsIgnoreCase(Tyre.CLINCHER.toString())) {
			for (Map.Entry<String, Double> entry : clincher_tyre_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (parts.equalsIgnoreCase(Tyre.TUBULAR.toString())) {
			for (Map.Entry<String, Double> entry : tubular_tyre_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}

		}
		return price;
	}

}
