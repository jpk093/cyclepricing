package com.web.cyclepricing.dao;

import com.web.cyclepricing.enums.SeatType;
import com.web.cyclepricing.utils.CustomStringUtils;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.HashMap;

@Repository
public class SeatDAO {

	private Map<String, Double> seat_comfort_price;
	private Map<String, Double> seat_ordinary_price;
	private Map<String, Double> seat_racing_price;

	public SeatDAO() {
		setSeatPrice();
	}

	private void setSeatPrice() {
		seat_comfort_price = new HashMap<>();
		seat_comfort_price.put("dec-2016", 2000.00);
		seat_comfort_price.put("jan-2016", 1500.00);

		seat_ordinary_price = new HashMap<>();
		seat_ordinary_price.put("dec-2016", 1000.00);
		seat_ordinary_price.put("jan-2016", 2000.00);

		seat_racing_price = new HashMap<>();
		seat_racing_price.put("dec-2016", 1000.00);
		seat_racing_price.put("jan-2016", 2000.00);

	}

	public Double getSeatPrice(String part, Long date) {
		Double price = Double.valueOf(0);

		if (part.equalsIgnoreCase(SeatType.ORDINARY.toString())) {

			for (Map.Entry<String, Double> entry : seat_comfort_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(SeatType.COMFORT.toString())) {

			for (Map.Entry<String, Double> entry : seat_ordinary_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(SeatType.RACING.toString())) {

			for (Map.Entry<String, Double> entry : seat_racing_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		}
		return price;
	}

}
