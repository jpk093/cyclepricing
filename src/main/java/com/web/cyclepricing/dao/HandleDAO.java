package com.web.cyclepricing.dao;

import com.web.cyclepricing.enums.HandleType;
import com.web.cyclepricing.utils.CustomStringUtils;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.HashMap;

@Repository
public class HandleDAO {

	private Map<String, Double> handle_ordinary_price;
	private Map<String, Double> handle_drop_price;
	private Map<String, Double> handle_randonneur_price;

	private Map<String, Double> handle_standard_price;
	private Map<String, Double> handle_track_price;
	
	private Map<String, Double> handle_shocklock_price;


	public HandleDAO() {
		setFramePrice();
	}

	private void setFramePrice() {
		handle_ordinary_price = new HashMap<>();
		handle_ordinary_price.put("dec-2016", 2222.00);
		handle_ordinary_price.put("jan-2016", 1111.00);

		handle_drop_price = new HashMap<>();
		handle_drop_price.put("dec-2016", 1000.00);
		handle_drop_price.put("jan-2016", 2000.00);

		handle_randonneur_price = new HashMap<>();
		handle_randonneur_price.put("dec-2016", 1000.00);
		handle_randonneur_price.put("jan-2016", 2000.00);

		handle_standard_price = new HashMap<>();
		handle_standard_price.put("dec-2016", 1000.00);
		handle_standard_price.put("jan-2016", 2000.00);

		handle_track_price = new HashMap<>();
		handle_track_price.put("dec-2016", 1000.00);
		handle_track_price.put("jan-2016", 2000.00);
		
		handle_shocklock_price = new HashMap<>();
		handle_shocklock_price.put("dec-2016", 100.00);
		handle_shocklock_price.put("jan-2016", 200.00);


	}

	public Double getHandlePrice(String part, Long date) {
		Double price = Double.valueOf(0);

		if (part.equalsIgnoreCase(HandleType.ORDINARY.toString())) {

			for (Map.Entry<String, Double> entry : handle_ordinary_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(HandleType.DROP.toString())) {

			for (Map.Entry<String, Double> entry : handle_drop_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(HandleType.RANDONNEUR.toString())) {

			for (Map.Entry<String, Double> entry : handle_randonneur_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(HandleType.STANDARD.toString())) {

			for (Map.Entry<String, Double> entry : handle_standard_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} else if (part.equalsIgnoreCase(HandleType.TRACK.toString())) {

			for (Map.Entry<String, Double> entry : handle_track_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} 
		return price;
	}
	
	
	public Double getShocklockPrice(Boolean isPart, Long date) {
		Double price = Double.valueOf(0);
		if (isPart) {
			for (Map.Entry<String, Double> entry : handle_shocklock_price.entrySet()) {
				if (entry.getKey().equals(CustomStringUtils.genKey(date)))
					price = entry.getValue();
			}
		} 
		return price;
	}

}
