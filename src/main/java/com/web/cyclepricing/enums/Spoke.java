package com.web.cyclepricing.enums;

public enum Spoke {
	
	TANGENTIALLACING, WHEELBUILDING;

}
