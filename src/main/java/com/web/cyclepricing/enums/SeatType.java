package com.web.cyclepricing.enums;

public enum SeatType {

	COMFORT, ORDINARY, RACING;

}
