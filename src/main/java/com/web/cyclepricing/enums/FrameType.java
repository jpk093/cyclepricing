package com.web.cyclepricing.enums;

public enum FrameType {
	
	CANTILEVER,DIAMOND,ORDINARY,PRONE,RECUMBENT,STEPTHROUGH;

}
