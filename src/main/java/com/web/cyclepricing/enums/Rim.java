package com.web.cyclepricing.enums;

public enum Rim {
	
	STEEL, ALLOY, CHROME;

}
