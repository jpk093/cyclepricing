package com.web.cyclepricing.enums;

public enum Tyre {

	CLINCHER, TUBULAR;
}
